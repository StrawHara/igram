//
//  LoginViewController.swift
//  iGram
//
//  Created by romain on 8/7/18.
//  Copyright © 2018 Romain Le Drogo. All rights reserved.
//

import UIKit
import WebKit
import Moya

final class LoginViewController: UIViewController, StoryboardBased {

    // MARK: Outlets
    @IBOutlet weak var webView: WKWebView!

    // MARK: Properties
    var appCoordinator: AppCoordinator?

    // MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()

        self.webView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
        self.loadInstagramAuthorization()
    }

    func setup(appCoordinator: AppCoordinator) {
        self.appCoordinator = appCoordinator
    }

    // MARK: Privates
    private func loadInstagramAuthorization() {
        let stringURL = Instagram.baseURL + Instagram.token.path +
            "/?client_id=\(Instagram.clientID)" +
            "&redirect_uri=\(Instagram.redirectURL)" +
            "&response_type=code"
        guard let url = URL(string: stringURL) else {
            return
        }

        self.webView.load(URLRequest(url: url))
    }

}

extension LoginViewController: WKNavigationDelegate {

    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                               change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {

        if keyPath == #keyPath(WKWebView.url) {
            guard let url = self.webView.url?.description else { return }

            if url.contains("http://ledrogo.fr/?code="),
                let range = url.range(of: "http://ledrogo.fr/?code=") {
                UserDefaults.standard.instagramAPIAuthorizationCode = String(url[range.upperBound...])
                self.appCoordinator?.authorizationCodeRetrieved()
            }
        }
    }

}

//- (void) webView: (WKWebView *) webView didStartProvisionalNavigation: (WKNavigation *) navigation {
//    NSLog(@"%s", __PRETTY_FUNCTION__);
//    NSURL *u1 = webView.URL;  //By this time it's changed
//}
