//
//  AppCoordinator.swift
//  iGram
//
//  Created by romain on 8/7/18.
//  Copyright © 2018 Romain Le Drogo. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator {

    internal var window: UIWindow?

    // MARK: Login
    internal var loginVC: LoginViewController

    // MARK: Application
    internal var splitViewController: UISplitViewController
    internal var listNav: UINavigationController
    internal var detailsNav: UINavigationController

    // MARK: Init
    init(window: UIWindow?) {
        self.window = window

        self.loginVC = LoginViewController.instantiate()

        let listVC = ListViewController.instantiate()
        self.listNav = UINavigationController(rootViewController: listVC)

        let detailsVC = DetailsViewController.instantiate()
        self.detailsNav = UINavigationController(rootViewController: detailsVC)

        self.splitViewController = UISplitViewController()
        self.splitViewController.viewControllers = [listVC, detailsVC]
    }

    // MARK: Coordinator implementation
    func start() {
        if UserDefaults.standard.instagramAPIAuthorizationCode?.isEmpty == false {
            self.getInstagramAuthorizationCode()
        } else {
            self.launch()
        }
    }

    func authorizationCodeRetrieved() {
        self.launch()
    }

    // MARK: Routes
    fileprivate func getInstagramAuthorizationCode() {
        self.loginVC.setup(appCoordinator: self)
        self.window?.rootViewController = self.loginVC
    }

    fileprivate func launch() {
        self.window?.rootViewController = self.splitViewController
    }

}
