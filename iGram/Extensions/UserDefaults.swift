//
//  UserDefaults.swift
//  iGram
//
//  Created by romain on 8/8/18.
//  Copyright © 2018 Romain Le Drogo. All rights reserved.
//

import Foundation

extension UserDefaults {

    var instagramAPIAuthorizationCode: String? {
        get { return string(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }

}
