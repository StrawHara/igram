//
//  InstagramWebServices.swift
//  iGram
//
//  Created by romain on 8/8/18.
//  Copyright © 2018 Romain Le Drogo. All rights reserved.
//

import Foundation
import Moya

// MARK: Constants
public enum Instagram {

    // Properties
    static let clientID = "1b12b8c3f7e142298feee46540bfefc3"
    static let baseURL = "https://api.instagram.com"
    static let redirectURL = "http://ledrogo.fr"

    static private let clientSecret = "d703e035a3f446868b31c676b96392a4"

    // Routes
    case token
//    case login(username: String, password: String)
}

extension Instagram: TargetType {

    public var baseURL: URL {
        return URL(string: Instagram.baseURL)!
    }

    public var path: String {
        switch self {
        case .token: return "/oauth/authorize"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .token: return .get
        }
    }

    public var sampleData: Data {
        return Data()
    }

    public var task: Task {

        let authParams = ["client_id": Instagram.clientID,
                          "redirect_uri": Instagram.redirectURL,
                          "response_type": "code"]

        switch self {
        case .token:
            return .requestParameters(
                parameters: authParams,
                encoding: URLEncoding.default)
        }
    }

    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }

    public var validationType: ValidationType {
        return .successCodes
    }
}
